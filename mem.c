#include "mem.h"

static struct mem* myHeap = NULL;

static struct mem init_mem_struct(struct mem* next, size_t capacity, bool is_free){
	struct mem returnstr;
	returnstr.next = next;
	returnstr.capacity = capacity;
	returnstr.is_free = is_free;
	return returnstr;
}

static uint64_t indeed_page_count(uint64_t need_size, uint64_t* remaining){
	//count indeeded page count and returns size of unused space in last page
	uint64_t result = need_size + sizeof(struct mem);
	if(result % HEAP_PAGE_SIZE != 0){
		*remaining = HEAP_PAGE_SIZE - (result % HEAP_PAGE_SIZE);
		result = (result/HEAP_PAGE_SIZE) + 1;
	}
	else{
		*remaining = 0;
		result /= HEAP_PAGE_SIZE;
	}
	return result;
}
static void *init_page(void *address, uint64_t size, bool fixed) {
	//Inits new page in memory via mmap
	return mmap(
        address,
        size,
        PROT_READ | PROT_WRITE,
        MAP_PRIVATE | MAP_ANONYMOUS | (fixed ? MAP_FIXED : 0),
        -1,
        0
    );
}
static void block_split(struct mem *splitted, uint64_t query) {
	struct mem *expanded = (struct mem*) ((char*)splitted + sizeof(struct mem) + query);
	*expanded =(struct mem) {splitted->next, splitted->capacity - sizeof(struct mem) - query, true};
	*splitted =(struct mem) {expanded, query, splitted->is_free};
}

static void merge_blocks(struct mem *merged, struct mem *extra) {
	*merged = (struct mem) {extra->next, merged->capacity + sizeof(struct mem) + extra->capacity, merged->is_free};
}

static bool sequential_blocks(struct mem *first, struct mem *next) {
    //Checks if blocks are sequential
	return (char*) next == (char*) first + sizeof(struct mem) + first->capacity;
}

static bool splittable_block(struct mem *splitted, uint64_t query) {
	//If block can be splitted
    return splitted->capacity >= query + sizeof(struct mem) + BLOCK_MIN_SIZE;
}
void* heap_init( size_t start_size )
{
	if(myHeap != NULL)return NULL;
	if(start_size < HEAP_PAGE_SIZE) start_size = HEAP_PAGE_SIZE;
	myHeap = init_page(HEAP_START, start_size, false);
	*myHeap = (struct mem) {NULL, start_size-sizeof(struct mem), true};
	#ifdef NEED_DEBUG_INFO
	printf("Heap initialization OK at %p, capacity: %lu\n", myHeap, start_size);
	#endif
	
}

void* _malloc( size_t query )
{
	#ifdef NEED_DEBUG_INFO
	printf("Malloc call to init %lu bytes: ", query);
	#endif
	struct mem *curChunk = HEAP_START;
	struct mem *lastChunk = NULL;
	void* predicatedAddres;
	uint64_t size_required = query + sizeof(struct mem);
	if(size_required < HEAP_PAGE_SIZE) size_required = HEAP_PAGE_SIZE;
	while(curChunk != NULL){
		if(curChunk->is_free){
			
			if(curChunk->capacity != query && splittable_block(curChunk, query)){
				block_split(curChunk, query);		
			}
			else{
				continue;
			}
			curChunk->is_free = false;
			#ifdef NEED_DEBUG_INFO
			printf("inited at %p without mmap call\n", curChunk);
			#endif
			return (char*)curChunk + sizeof(struct mem);
		}
		lastChunk = curChunk;
		curChunk = curChunk->next;
	}
	predicatedAddres = (char*)lastChunk + sizeof(struct mem) + lastChunk->capacity;
	curChunk = init_page(predicatedAddres, size_required, true);
	if(curChunk == MAP_FAILED){
		curChunk = init_page(predicatedAddres, size_required, false);
		if(curChunk == MAP_FAILED){
			#ifdef NEED_DEBUG_INFO
			printf("failed, can't init page in memory\n");
			#endif
			return NULL;
		}
	}
	lastChunk->next = curChunk;
	*curChunk = (struct mem) {NULL, size_required-sizeof(struct mem), false};
	if( splittable_block(curChunk, query))
		block_split(curChunk, query);
	#ifdef NEED_DEBUG_INFO
	printf("inited at %p with mmap call\n", curChunk);
	#endif
	return (char*) curChunk + sizeof(struct mem);
}
void _free( void* mem ){
	#ifdef NEED_DEBUG_INFO
	printf("Free call to release at %p: ", mem);
	#endif
	struct mem *header = (struct mem*) ((char*)mem - sizeof(struct mem));
	struct mem *chunkBefore,*chunkNext;
	if(!mem)return;
	if(header != myHeap){
		for(chunkBefore = myHeap; chunkBefore->next != NULL&&chunkBefore->next != header; chunkBefore = chunkBefore->next);
		if(chunkBefore->next != header){
			#ifdef NEED_DEBUG_INFO
			printf("error(chunk isn't in start and there are no chunk, pointing at it)\n");
			#endif
			return;
		}
	}
	header->is_free = true;
	chunkNext = header->next;
	if(chunkNext != NULL&&chunkNext->is_free&&sequential_blocks(header, chunkNext))
		merge_blocks(header, chunkNext);
	if(chunkBefore != NULL&&chunkBefore->is_free&&sequential_blocks(chunkBefore, header))
		merge_blocks(chunkBefore, header);
	#ifdef NEED_DEBUG_INFO
	printf("OK\n");
	#endif
}
void* _realloc(void *ptr, size_t query){
	struct mem *header = (struct mem*) ((char*)ptr - sizeof(struct mem));
	if(header == NULL)return NULL;
	struct mem *preChunk = HEAP_START;
	size_t newcapacity = header->capacity + query;
	for(;preChunk&&preChunk->next != header; preChunk = preChunk->next){
		
	}
	if(header->next&&header->next->capacity + sizeof(struct mem) >= query&& sequential_blocks(header, header->next)){
		printf("merged with before\n");
		merge_blocks(header, header->next);
		if(splittable_block(header, newcapacity)){
			block_split(header, newcapacity);
		}
		return (char*)header + sizeof(struct mem);
	}
	else if(preChunk&& preChunk->capacity + sizeof(struct mem) >= query && sequential_blocks(preChunk, header)) {
		printf("merged with after\n");
		struct mem originCopy = {header->next, header->capacity, header->is_free};
		memcpy(preChunk + sizeof(struct mem) + preChunk->capacity, header + sizeof(struct mem), header->capacity);
		preChunk->capacity += originCopy.capacity + sizeof(struct mem);
		preChunk->next = originCopy.next;
		if(splittable_block(preChunk, newcapacity)){
			block_split(preChunk, newcapacity);
		}
		return (char*)preChunk + sizeof(struct mem);
	}
	else{
		printf("reallocated in new place\n\n");
		void* newdata = _malloc(newcapacity);
		memcpy(newdata, header + sizeof(struct mem), header->capacity);
		_free(ptr);
		return (char*)newdata;
	}
}

