#include "mem.h"
struct teststruct{
	int value;
	struct teststruct* anyLink;
};
int main(){
	heap_init(HEAP_PAGE_SIZE);
	struct teststruct *structTesting;
	int *singleint;
	int *intarray;
	int *intToBeMerged;
	int i;
	singleint = _malloc(sizeof(int));
	intarray = _malloc(sizeof(int)*10);
	intToBeMerged = _malloc(sizeof(int));
	printf("sizeof : %d\n", sizeof(struct mem));
	//printf("=================================================\nHeap info after alloc:\n");
	//memalloc_debug_heap(stdout);
	//printf("=================================================\n");
	for(i = 0; i< 10; i++){
		intarray[i] = rand();
	}
	*singleint = 20;
	*intToBeMerged = 500;
	structTesting =  _malloc(sizeof(struct teststruct) * 4);
	memalloc_debug_heap(stdout);
	printf("%p", structTesting );
	structTesting[0] = (struct teststruct) {20, structTesting+1};
	structTesting[1] = (struct teststruct) {591241, structTesting+2};
	structTesting[2] = (struct teststruct) {-228,  structTesting+3};
	structTesting[3] = (struct teststruct) {49,  structTesting};
	//printf("=================================================\nHeap info before realloc:\n");
	//memalloc_debug_heap(stdout);
	//printf("=================================================\n");
	structTesting = _realloc(structTesting, sizeof(struct teststruct) * 5);
	printf("%p", structTesting);
	structTesting[4] = (struct teststruct) {49,  structTesting};
	
	//printf("=================================================\nHeap info after memfill:\n");
	//memalloc_debug_heap(stdout);
	//printf("=================================================\n");
	
	
	_free(intToBeMerged);
	//printf("=================================================\nHeap info after first free:\n");
	//memalloc_debug_heap(stdout);
	//printf("=================================================\n");
	_free(intarray);
	_free(structTesting);
	//printf("=================================================\nHeap info after second free:\n");
	//memalloc_debug_heap(stdout);
	//printf("=================================================\n");
	_free(singleint);
	//printf("=================================================\nHeap info after all free\n");
	//memalloc_debug_heap(stdout);
	//printf("=================================================\n");
	return 0;
}