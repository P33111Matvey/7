all: program

program: main.c mem.c mem_debug.c
	gcc -g -pedantic-errors -o main main.c mem.c mem_debug.c

run: program
	./main

clean:
	rm main
